# Méta Arbrifest

_Quelques notes sur l'activité d'arbrifest que nous avons réalisé le 7 sptembre 2018_

- C'est intéressant de partir d'une matière existante (un atelier précédent)
- Venir avec moins d'intention pour laisser émerger des choses plus variés
- L'étape de redaction des principes permet de crystaliser les discussions autour des valeurs
- C'est bien que les valeurs sont un peu frustrante, c'est peut-être parce que c'est frustrant que les étapes suivantes sont intéressante, plus riche.
- Faut-il annoncer au premier atelier que c'est un premier atelier, et que le resultat sera frustrant peut-être... Que ce résultat sera re-utilisé.
- Série d'atelier possible pour tourner autour des valeurs; avoir des discussions en l'air, jus'quà ce que nous soyons prêt pour passer à l'étape des principes.
- Est-ce qu'en fait les valeurs sont des fruits, et les fruits sont les racines ? Faire un arbrifest pourrait se faire en partant des pratiques actuelles pour en déduire des principes et en déduire ensuite des valeurs.
- Un abrifest c'est pour faire apparaitre la zone d'intersection des valeurs / principes des personnes participantes