# Les valeurs de « La Zone »

## Horizontalité
- pas de hiérarchie >> safe, plus de solidarité
- tou·te·s apprenant·e·s >> multidisciplinarité, communauté d'aprentissage
- tout le monde est au même niveau d'information >> efficacité, fluidité, confiance
- s'il y a hiérarchie ou leadership alors çà tourne >> empathie, équilibre des pouvoirs

## Écoute
- tout le monde peut s'exprimer (notamment les timides ou qui ont besoin de temps pour se forger une opinion) >> diversité d'opinion, collaboratif
- ne pas arriver avec beaucoup d'intention : être ouvert à ce que des choses puissent se produire >> plus d'options, moins de croyances
- laisser des avis différents s'exprimer >> pluralité (plus d'options), moins de frustration
- favoriser l'inclusion ou la disparité (écoute large) >> moins d'entre-soi, créativité, prise de conscience des privilèges
- prendre le temps de lire et de comprendre ce que d'autre évoquent (exemple sur slack) >> utiliser le média le plus adapté au contexte, pertinence
- passer à l'oral quand on a des doutes à l'écrit, et vice-versa >> Moins d'incompréhension (moins de conflits) 

## Transparence
- Documenter pour transmettre (à l'extérieur, la culture interne étant déjà collaborative) >> facilité l'accès des nouve·aux·lles arrivant·e·s, mémoire, accessibilité 
- Pour inspirer (toujours à l'extérieur) >> reproductibilité, essaimer, être moins isolé·e
- Provoquer la réciprocité >> aide les personnes à participer (invitation), 
- Mettre en exergue la transparence chez les autres >> vulgarisation, banalisation de la transparence)
- Rendre accessible les données brut qu'on pourrait présenter en transparence >> Rendre compréhensible, abaisser la barrière à l'entrée, dé-jargonner, démocratiser

## Solidarité
- Quand quelqu'un·e est dans la merde on essaye de l'aider >> Aide à prendre des risques, aier à passer des moments difficiles, préférer le faire ensemble à l’efficacité individuelle, résilience
- Solidarité entre membres du groupe vis à vis de l'extérieur >> Améliore la communication avec l'extérieur
- Solidarité entre collaborat·eur·rice·s >> privilégier la qualité de vie, serenité


Pour la prochaine fois ?

Date : Lundi : 10h
Todo :
    - début d'arbrifeste - dessin
    - rédiger méta-arbrifeste
    - cpmmencer backlog pour AO-ST-NC ?
    