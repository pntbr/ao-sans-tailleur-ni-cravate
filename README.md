# Sans tailleur ni cravate

Lorsqu'on répond à une solicitation, la tentation de s'acculturer et de séduire pour "gagner" est forte.

On voudrait tester l'hypothèse suivante : Et si, on répondait sincèrement ? 


## Qui pourrait utiliser ce qui va être produit ?

1 - Les entreprises "sympas" IT qu'on connait
  - DTC
  - scopyleft
  - Codeurs en liberté

2 - Les entreprises "sympas" pas IT connues
  - Les vigies
  - La Cagette
  - Cedrats

3 - Des indépendant·e·s
  - Mélia
  - Maïtané Lenoir
  - Pascal Romain

## Quels sont les problèmes ?

- Je ne suis pas à l'aise pour répondre à des solicitations en faisant le/a malin·e.
- En adaptant ma réponse j'endommage la relation de confiance.
- Je ne sais pas si répondre sans m'adapter aux attentes est possible.

## L'intention

- Apprendre à répondre en restant droit·e dans ses bottes.
- Lister des formats de réponses et des exemples.

## Ça pourrait ressembler à quoi ?

- Un document qui expliquerait nos valeurs, notre éthique.
  - Vision (arbrifeste)
  - Code of Conduct
- Un livre blanc pour expliquer nos pratiques.
  - Construction d'un produit
  - Comment ils définissent leurs prix
  - Foire Aux Questions
  
- Définir un cadre minimal p notre réponse au contexte
  - Reformulation de la problèmatique et du contexte
  - Intérêt pour le contexte
  - Backlog et plan d'itération

## Pourquoi c'est à nous de le faire ?

- On passe notre temps à tenter des nouveaux trucs
- On est identifié atypique
- On est sensibles qu'en il s'agit de faire des choses non-justifiées
